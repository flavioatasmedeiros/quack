/** @jsx React.DOM */

var Playlist = React.createClass({

  componentWillMount: function() {
    this.quack = null;
    this.players = {};
  },

  getInitialState: function() {
    return {loader: {display: 'none'}};
  },

  render: function() {
    return <ul>
      {this.props.children.map(function(data, i) {
        return <Quack data={data} key={i} />
      })}
      <div className="loader" style={this.state.loader} />
    </ul>
  },

  play: function(quack) {
    var videoId = quack.props.data.video;
    var playAndHide = _.partial(quack.play, this.hideLoader);
    
    this.showLoader();
    
    if (this.players[videoId]) {
      playAndHide();
    }
    else {
      quack.makePlayer(playAndHide);
      this.players[videoId] = quack;
    }
    
    this.quack = this.players[videoId];
  },

  hideLoader: function() {
    this.setState({loader: {display: 'none'}});
  },

  showLoader: function() {
    this.setState({loader: {display: 'block'}});
  }

});
