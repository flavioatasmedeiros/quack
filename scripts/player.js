function onYouTubeIframeAPIReady() {}


var Player = function(data) {
  _.extend(this, Backbone.Events);
  this.data = data;
  this.instance = this.getInstance();
};


Player.prototype.onReady = function(callback) {
  this.on('player:ready', callback);
};


Player.prototype.onStart = function(callback) {
  this.on('player:start', callback);
};


Player.prototype.onTimeChange = function(callback) {
  this.on('player:current:time', callback);
};


Player.prototype.destroy = function() {
  this.stop();
  this.instance.destroy();
};


Player.prototype.stop = function() {
  clearInterval(this.interval);
  this.instance.stopVideo();
  this.trigger('player:stop');
};


Player.prototype.play = function() {
  this.instance.playVideo();
  this.trigger('player:play');
};


Player.prototype.playInterval = function(start, end, callback) {
  this.onStart(startMonitoring);
  this.instance.seekTo(start);
  function startMonitoring() {
    this.onTimeChange(stopIfEnd);
  }
  function stopIfEnd(currentTime) {
    if (currentTime > end) {
      callback();
    }
  }
};


Player.prototype.startInterval = function(ms) {
  this.interval = setInterval(function() {
    this.trigger(
      'player:current:time', 
      this.instance.getCurrentTime()
    );
  }.bind(this), ms || 100);
};


Player.prototype.getInstance = function() {
  var id = 'player_' + this.data.video;
  
  $('body').prepend('<div id="' + id + 
    '"  style="display:none"></div>');
  
  return new YT.Player(id, {
    videoId: this.data.video, 
    events: {
      onReady: function() {
        this.trigger('player:ready');
      }.bind(this),
      onStateChange: function(ev) {
        if (ev.data === 1) {
          this.trigger('player:start');
          this.startInterval();
        }
      }.bind(this)
    }
  });
};
