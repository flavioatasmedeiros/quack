/** @jsx React.DOM */

loadData(function(response) {
  React.renderComponent(
    <Playlist>
      {_.values(response)}
    </Playlist>, 
    document.getElementById('list')
  );
});

React.renderComponent(
  <QuackForm>
    <Field name="video" required="true" value="" />
    <Field name="start" required="true" value="0" />
    <Field name="end" required="true" />
    <Field name="description" required="true" />
  </QuackForm>, 
  document.getElementById('form')
);
