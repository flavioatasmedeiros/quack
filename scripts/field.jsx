/** @jsx React.DOM */

var Field = React.createClass({

  getInitialState: function() {
    return {value: this.props.value || ''};
  },

  render: function() {
    return <li>
      <label>
        {this.props.label || this.props.name}
      </label>
      <input value={this.state.value} onChange={this.handleChange} type="text" />
    </li>
  },

  handleChange: function(ev) {
    this.setState({value: ev.target.value});
  }
  
});
