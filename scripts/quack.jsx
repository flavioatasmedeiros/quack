/** @jsx React.DOM */

var Quack = React.createClass({

  componentWillMount: function() {
    this.player = null;
  },

  getInitialState: function() {
    return {active: false};
  },

  handleClick: function() {
    this.props.__owner__.play(this);
  },

  render: function() {
    var className = this.state.active ? 'active' : '';
    return <li onClick={this.handleClick} className={className}>
      <p>
        {this.props.data.description}
      </p>
    </li>;
  },

  makePlayer: function(callback) {
    this.player = new Player(this.props.data);
    this.player.onReady(callback);
    return this.player;
  },

  play: function(callback) {
    this.player.onStart(function() {
      this.setState({active: true});
      callback();
    }.bind(this));
    this.player.playInterval(
      this.player.data.start, 
      this.player.data.end,
      this.stop
    );
  },

  stop: function() {
    this.player.stop();
    this.setState({active: false});
  }
  
});
