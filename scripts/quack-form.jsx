/** @jsx React.DOM */

var QuackForm = React.createClass({

  getInitialState: function() {
    return {style: {display: 'none'}};
  },

  componentWillMount: function() {
    this.errors = [];
  },

  render: function() {
    return <div className="form">
      <ul style={this.state.style}>
        {this.props.children}
        <li>
          <button onClick={this.submit}>
            Enviar
          </button>
        </li>
      </ul>
      <button onClick={this.preview} style={this.state.style}>
        Preview
      </button>
      <button onClick={this.toggle}>
        {this.isVisible() ? 'Cancelar' : 'Novo'}
      </button>
    </div>;
  },

  submit: function() {
    this.validateFields();
    if (this.errors.length) {
      alert(this.errors.join('\n'));
      return;
    }
    saveData(this.consolidateFields());
    this.toggle();
    this.clear();
  },

  validateFields: function() {
    this.errors = [];
    this.props.children.forEach(function(item) {
      if (item.props.required && item.state.value === '') {
        this.errors.push(item.props.name + ' is required');
      }
    }.bind(this));
  },

  consolidateFields: function() {
    return this.props.children.reduce(function(item, field) {
      item[field.props.name] = field.state.value;
      return item;
    }, {});
  },

  isVisible: function() {
    return this.state.style.display !== 'none';
  },

  preview: function() {
    var data = this.consolidateFields();
    var player = new Player(data);
    player.onReady(function() {
      player.playInterval(data.start, data.end, function() {
        player.stop();
      });
    });
  },

  toggle: function() {
    var style = {display: this.isVisible() ? 'none' : 'block'};
    this.setState({style: style});
  },

  clear: function() {
    this.props.children.forEach(function(field) {
      field.setState({value: ''});
    });
  }

});
